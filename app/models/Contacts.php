<?php

namespace App\Models;
use Core\Model;
use Core\Validators\{RequiredValidator, EmailValidator, UniqueValidator};

class Contacts extends Model {

  protected static $_table = 'contacts';
  public $id, $created_at, $updated_at, $user_id, $fname, $lname, $email;
  public $phone, $street, $street2, $city, $state, $zip_code, $deleted = 0;

  public function validator() {
    $this->runValidation(
      new RequiredValidator($this,['field'=>'fname','msg'=>'First Name is required.'])
    );

    $this->runValidation(
      new RequiredValidator($this,['field'=>'lname','msg'=>'Last Name is required.'])
    );

    $this->runValidation(
      new EmailValidator($this,['field'=>'email','msg'=>'You must provide a valid email address.'])
    );

    $this->runValidation(
      new UniqueValidator($this,['field'=>['email','user_id'],'msg'=>'That email is used by another contact.'])
    );
  }

  public function beforeSave() {
    $this->timestamps();
  }

  public static function findByUserId($user_id){
    $params = [
      'conditions' => "user_id = ?",
      'bind' => [$user_id],
      'order' => 'lname, fname'
    ];
    return self::find($params);
  }

  public function fullName(){
    return $this->fname . ' ' . $this->lname;
  }

  public static function findByIdAndUserId($id, $user_id){
    $params = [
      'conditions' => "id = ? AND user_id = ?",
      'bind' => [$id,$user_id]
    ];
    return self::findFirst($params);
  }

}