<?php $this->setSiteTitle("My Contacts"); ?>

<?php $this->start('body') ?>

<h2 class="text-dark">My Contacts</h2>

<table class="table table-bordered table-dark table-hover">
  <thead>
    <th>Name</th>
    <th>Phone</th>
    <th>Email</th>
    <th>City</th>
    <th>State</th>
    <th>Actions</th>
  </thead>
  <tbody>
    <?php foreach($this->contacts as $contact):?>
      <tr data-id="<?= $contact->id ?>">
        <td><?= $contact->fullName() ?></td>
        <td><?= $contact->phone ?></td>
        <td><?= $contact->email ?></td>
        <td><?= $contact->city ?></td>
        <td><?= $contact->state ?></td>
        <td>
          <a href="<?=PROOT?>contacts/edit/<?=$contact->id?>" class="text-light mr-1">
            <i class="fas fa-edit"></i>
          </a>
          <a href="#" onclick="deleteContact('<?=$contact->id?>');return false;" class="text-danger">
            <i class="fas fa-trash-alt"></i>
          </a>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<script>
  function deleteContact(id){
    var warn = "Are you sure you want to delete this message?"
    if(confirm(warn)){
      var xhr = jQuery.ajax({
        method : "POST",
        data : {contact_id : id},
        url : '<?=PROOT?>contacts/delete'
      });
      xhr.done(function(resp){
        var alertType = (resp.success)? 'info' : 'danger';
        alertMsg(resp.msg, alertType);
        if(resp.success){
          jQuery("tr[data-id='"+resp.id+"']").remove();
        }
      });
    }
  }
</script>

<?php $this->end() ?>