<?php use Core\FH; ?>

<?php $this->start('body') ?>
  
<div class="row align-items-center justify-content-center">
  <div class="col-md-10 bg-light p-5">
    <h3 class="text-center"><?= $this->header ?></h3>
    <form class="form" action="" method="post">
      <?= FH::csrfInput() ?>
      <div class="row">
        <?= FH::inputBlock('text','First Name','fname',$this->contact->fname,['class'=>'form-control'],['class'=>'form-group col-md-6'],$this->formErrors) ?>
        <?= FH::inputBlock('text','Last Name','lname',$this->contact->lname,['class'=>'form-control'],['class'=>'form-group col-md-6'],$this->formErrors) ?>
        <?= FH::inputBlock('text','Email','email',$this->contact->email,['class'=>'form-control'],['class'=>'form-group col-md-6'],$this->formErrors) ?>
        <?= FH::inputBlock('text','Phone','phone',$this->contact->phone,['class'=>'form-control'],['class'=>'form-group col-md-6'],$this->formErrors) ?>
        <?= FH::inputBlock('text','Address','street',$this->contact->street,['class'=>'form-control'],['class'=>'form-group col-md-6'],$this->formErrors) ?>
        <?= FH::inputBlock('text','Address Line 2','street2',$this->contact->street2,['class'=>'form-control'],['class'=>'form-group col-md-6'],$this->formErrors) ?>
        <?= FH::inputBlock('text','City','city',$this->contact->city,['class'=>'form-control'],['class'=>'form-group col-md-4'],$this->formErrors) ?>
        <?= FH::inputBlock('text','State','state',$this->contact->state,['class'=>'form-control'],['class'=>'form-group col-md-4'],$this->formErrors) ?>
        <?= FH::inputBlock('text','Postal Code','zip_code',$this->contact->zip_code,['class'=>'form-control'],['class'=>'form-group col-md-4'],$this->formErrors) ?>
      </div>
      <div class="d-flex justify-content-end">
        <a href="<?=PROOT?>contacts" class="btn btn-secondary mr-2">Cancel</a>
        <?= FH::submitTag('Save',['class'=>'btn btn-primary']) ?>
      </div>
    </form>
  </div>
</div>

<?php $this->end() ?>